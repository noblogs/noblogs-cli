<?php


// Return a list of all blogs (only blog_id and domain attrs).
// (Does not include deleted and archived blogs).
function noblogs_get_blogs() {
  global $wpdb;
  $sql = "SELECT blog_id, domain FROM $wpdb->blogs WHERE deleted = 0 AND archived = 0";
  $result = $wpdb->get_results($sql);
  return ($result);
}

// Return a list of local blog objects (only blog_id and domain attrs).
function noblogs_get_local_blog_objects() {
    global $wpdb;
    $blog_ids = noblogs_get_local_blogs();
    $n = count($blog_ids);
    $blogs = array();
    for ($i = 0; $i < $n; $i += 100) {
        $ids = array_slice($blog_ids, $i, 100);
        $sql = "SELECT blog_id, domain FROM $wpdb->blogs WHERE deleted = 0 AND archived = 0 AND blog_id IN (" . implode(",", $ids) . ")";
        $result = $wpdb->get_results($sql);
        array_push($blogs, ...$result);
    }
    return $blogs;
}

// Return the blog ID, if the argument is a blog name. If it's already
// an ID, return it unchanged.
function noblogs_get_blog_id($arg) {
    if (preg_match('/^\d+$/', $arg)) {
        return $arg;
    }
    $blog = noblogs_get_blog($arg);
    if (!$blog) {
        return -1;
    }
    return $blog->blog_id;
}

// Find a blog by its name or ID.
function noblogs_get_blog($blogname) {
  global $wpdb;
  if (preg_match('/^\d+$/', $blogname)) {
    $sql = "SELECT * FROM $wpdb->blogs WHERE blog_id='" . $blogname . "'";
  } else {
    if (!preg_match('/\.noblogs\.org$/', $blogname)) {
      $blogname = $blogname . '.noblogs.org';
    }
    $sql = "SELECT * FROM $wpdb->blogs WHERE domain = '" . $blogname . "'";
  }
  $result = $wpdb->get_results($sql);
  return $result[0];
}

// Return the database connection information associated with a blog.
function noblogs_get_backend_for_blog($blog_id) {
  global $wpdb;
  global $r2db_hash_map;
  global $wpdb_reverse_backend_map;

  // Compatibility shim between old/new r2db.
  $hash_map = $r2db_hash_map;
  if (!$hash_map) {
    $hash_map = $wpdb->hash_map;
  }

  // Lookup the blog ID.
  $lookup = $hash_map->lookup($blog_id);
  $backend = $wpdb_reverse_backend_map[$lookup];

  // Be nice and split the database host into 'host' and 'port' elements.
  $result = array(
                  "shard" => $lookup,
                  "user" => $backend['user'],
                  "password" => $backend['password'],
                  "db" => $backend['name']
                  );
  if (preg_match('/^(.*):([0-9]*)$/', $backend['host'], $matches)) {
    $result['host'] = $matches[1];
    $result['port'] = $matches[2];
  } else {
    $result['host'] = $backend['host'];
  }
  return $result;
}

// Return the full backend -> blogs map.
function noblogs_get_backend_map() {
  global $wpdb;
  global $r2db_hash_map;

  // Compatibility shim between old/new r2db.
  $hash_map = $r2db_hash_map;
  if (!$hash_map) {
    $hash_map = $wpdb->hash_map;
  }

  $blogs = noblogs_get_blogs();
  $backend_map = array();

  foreach ($blogs as $blog) {
    $blog_id = $blog->blog_id;
    $backend_id = $hash_map->lookup($blog_id);
    if (!array_key_exists($backend_id, $backend_map)) {
      $backend_map[$backend_id] = array();
    }
    array_push($backend_map[$backend_id], (int)$blog_id);
  }

  return $backend_map;
}

// Return the backend ID for the local host.
function noblogs_get_local_backend_id() {
  global $noblogs_config;
  return $noblogs_config['local_backend_name'];
}

// Return a list of blogs that are local to this server.
function noblogs_get_local_blogs() {
  $backend_map = noblogs_get_backend_map();
  $local_id = noblogs_get_local_backend_id();
  return $backend_map[$local_id];
}

// Return true if the blog has not been updated in some time.
function noblogs_is_stale($days = 180) {
    global $wpdb;
    $recent = new WP_Query("showposts=1&orderby=modified&post_status=publish");
    if ($recent->have_posts()) {
        $recent->the_post();
        $last_update = get_the_modified_time('U');
        $now = time();
        if ( ($now - $last_update) > 86400*$days )
            return true;
    }
    return false;
}
