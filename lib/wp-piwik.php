<?php

function do_wp_piwik_clear_config($args){

    $wp_piwik_options_to_delete = array(
        'wp-piwik_displayto',
        'wp-piwik_revision',
        'wp-piwik-track_404'
    );

    $wp_piwik_options_to_blank = array(
        'wp-piwik-name',
        'wp-piwik-noscript_code',
        'wp-piwik-site_id',
        'wp-piwik-tracking_code'
    );

    foreach ($args as $arg) {
        $blog = noblogs_get_blog($arg);
        if (!$blog) {
            echo "Blog {$arg} not found.\n";
            continue;
        }
        switch_to_blog($blog->blog_id);
        echo "Clearing options for wp-piwik for blog {$arg}\n";

        foreach ($wp_piwik_options_to_delete as $opt) {
            if (get_option($opt)){
                echo "Deleting option {$opt}\n";
                delete_option($opt);
            }
        }
        foreach ($wp_piwik_options_to_blank as $opt) {
            echo "Clearing option {$opt}\n";
            update_option($opt,'');
        }

        restore_current_blog();
    }
}

// [*] functioning site
//  *  wp-piwik-dashboard_revision
//     wp-piwik_displayto
//  *  wp-piwik-last_tracking_code_update
//  *  wp-piwik-name
//  *  wp-piwik-noscript_code
//     wp-piwik_revision
//  *  wp-piwik-site_id
//     wp-piwik-track_404
//  *  wp-piwik-tracking_code
//         +------------------------------------+--------------+
//         | option_name                        | option_value |
//         +------------------------------------+--------------+
//         | wp-piwik-dashboard_revision        | 0            |
//         | wp-piwik-last_tracking_code_update | 1541989337   |
//         | wp-piwik-name                      |              |
//         | wp-piwik-noscript_code             |              |
//         | wp-piwik-site_id                   |              |
//         | wp-piwik-tracking_code             |              |
//         +------------------------------------+--------------+
