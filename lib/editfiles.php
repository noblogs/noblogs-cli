<?php

/* Comments/uncomments a section of a file, placed between BEGIN and
   END markers. */
function comment_with_markers($filename, $marker, $do_comment) {
  if (!file_exists($filename)) {
    return false;
  }
  $markerdata = explode("\n", implode('', file($filename)));
  if (!$f = @fopen($filename, 'w')) {
    return false;
  }
  $foundit = false;

  $begin_marker = '# BEGIN ' . $marker;
  $end_marker = '# END ' . $marker;

  if ($markerdata) {
    $state = false;
    foreach ($markerdata as $n => $markerline) {
      if (strpos($markerline, $end_marker) !== false) {
        $state = false;
      }

      if ($state) {
        if ($do_comment) {
          if (substr($markerline, 0, 1) != '#') {
            $markerline = '#' . $markerline;
          }
        } else {
          if (substr($markerline, 0, 1) == '#') {
            $markerline = substr($markerline, 1);
          }
        }
      }

      if ($n + 1 < count($markerdata)) {
        fwrite($f, "{$markerline}\n");
      } else {
        fwrite($f, "{$markerline}");
      }

      if (strpos($markerline, $begin_marker) !== false) {
        $state = true;
        $foundit = true;
      }
    }
  }

  fclose($f);
  return $foundit;
}
