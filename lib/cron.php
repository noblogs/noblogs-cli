<?php

function is_hook_safe($hook) {
    if (strlen($hook) > 2 && substr($hook, 0, 3) == "wp_") {
        return true;
    }
    if ($hook == "delete_expired_transients") {
        return true;
    }
    return false;
}

// Run cron jobs for the current blog.
// (Use switch_to_blog() before calling this).
//
// We can deal with core Wordpress events inline, but not with events
// associated with plugins, as they aren't properly loaded when
// switch_to_blog() is called. So we detect when "unsafe" events
// should be scheduled, and only for those blogs we spawn a wp-cli
// process (much slower) to run them in the specific context of the
// blog.
function noblogs_run_cron_for_current_blog() {
  $crons = wp_get_ready_cron_jobs();

  $has_unsafe_hooks = false;
  foreach ($crons as $timestamp => $cronhooks) {
    foreach ($cronhooks as $hook => $keys) {
      if (!is_hook_safe($hook)) {
        $has_unsafe_hooks = true;
        continue;
      }
      foreach ($keys as $k => $v) {
        echo "  {$k} -> {$hook}()\n";
        $schedule = $v['schedule'];
        if ($schedule) {
            $result = wp_reschedule_event($timestamp, $schedule, $hook, $v['args'], true);
            if (is_wp_error($result)) {
                error_log(sprintf(
                    'cron reschedule event error for hook: %1$s, code: %2$s, message: %3$s, data: %4$s',
                    $hook, $result->get_error_code(), $result->get_error_message(), wp_json_encode($v)));
                do_action('cron_reschedule_event_error', $result, $hook, $v);
            }
        }
        $result = wp_unschedule_event($timestamp, $hook, $v['args'], true);
        if (is_wp_error($result)) {
            error_log(sprintf(
                'cron unschedule event error for hook: %1$s, code: %2$s, message: %3$s, data: %4$s',
                $hook, $result->get_error_code(), $result->get_error_message(), wp_json_encode($v)));
            do_action('cron_unschedule_event_error', $result, $hook, $v);
        }
        do_action_ref_array($hook, $v['args']);
      }
    }
  }

  if ($has_unsafe_hooks) {
    $blog_details = get_blog_details(null, false);
    $domain = $blog_details->domain;
    echo "  executing wp-cli for unsafe hooks ({$domain})\n";
    system("wp cron event run --due-now --url={$domain} 2>/dev/null", $retval);
    if ($retval != 0) {
        error_log("error executing wp-cli cron runner for blog {$domain}");
    }
  }
}
