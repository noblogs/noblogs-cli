
Tool di gestione di Noblogs
===========================


Per qualche ulteriore informazione, lanciare il comando `noblogs`
senza argomenti, mostrera' un piccolo help.

Alcuni esempi di come usare i tool in `bin/`:

* Aggiornare tutti i blog dopo un upgrade di WP:

    $ on-all-blogs upgrade

* Aggiornare un singolo blog:

    $ noblogs upgrade cavallette

* Lanciare i cron job per i blog che sono ospitati sul server locale:

    $ on-local-blogs run-cron


## Installazione

Per installare questo software manualmente, assumendo che l'installazione
di WordPress si trovi in `$WPDIR`:

    $ autoconf
    $ ./configure --with-wp-root=$WPDIR
    $ sudo make install

